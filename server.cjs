const express = require('express');
const path = require('path');

const app = express();

app.use(express.static('public'));

app.set('view engine', 'ejs');

app.get('/login', (req, res) => {
  res.render('autorization');
});
app.get('/main', (req, res) => {
  res.render('first');
});

const port = 5000;

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
