import React from 'react';
import ReactDOM from 'react-dom';

class Hello extends React.Component {
    render() {
      return React.createElement('div', null, `Hello ${this.props.toWhat}`);
    }
  }